import CartParser from './CartParser';

let parser;

beforeEach(() => {
  parser = new CartParser();
});

describe('CartParser - parse', () => {
  it('should throw error when string is not path to file', () => {
    expect(() => {
      parser.parse('randomstring');
    }).toThrow("ENOENT: no such file or directory, open 'randomstring'");
  });
});

describe('CartParser - parseLine', () => {
  it('should return object with specific field', () => {
    const line = ``;
    const result = parser.parseLine(line);
    expect(result).toHaveProperty('id');
    expect(result).toHaveProperty('name', '');
    expect(result).toHaveProperty('price', NaN);
    expect(result).toHaveProperty('quantity', NaN);
  });

  it('should throw error when argument is not string', () => {
    const line = [[undefined, 0, 'dddfd'], undefined, 0, {}, null];
    for (const item in line) {
      try {
        parser.validate(line[item]);
      } catch (e) {
        expect(e).toBeInstanceOf(Error);
      }
    }
  });

  it('should throw error when argument is not define', () => {
    try {
      parser.validate();
    } catch (e) {
      expect(e).toBeInstanceOf(Error);
    }
  });
});

describe('CartParser - calcTotal', () => {
  it('should return correct value', () => {
    expect(parser.calcTotal([{ price: 0, quantity: 'string' }])).toBe(NaN);
    expect(
      parser.calcTotal([
        { price: 0, quantity: '3' },
        { price: 3, quantity: 3 },
        { price: 5, quantity: 0.2 }
      ])
    ).toBe(10);
    expect(parser.calcTotal([{}])).toBe(NaN);
  });

  it('should return 0 when array is empty', () => {
    expect(parser.calcTotal([])).toBe(0);
  });
});

describe('CartParser - validate', () => {
  it('should throw error when content is empty', () => {
    try {
      parser.validate();
    } catch (e) {
      expect(e).toBeInstanceOf(Error);
    }
  });

  it('should throw error when content is not string', () => {
    const testData = [[undefined, 0, 'dddfd'], undefined, 0, {}, null];
    for (const item in testData) {
      try {
        parser.validate(testData[item]);
      } catch (e) {
        expect(e).toBeInstanceOf(TypeError);
      }
    }
  });

  it('should return error array when content don`t have all valid arguments', () => {
    let res1 = parser.validate('Not valid value');
    expect(res1.length).toBeGreaterThan(0);
    expect(res1).toBeInstanceOf(Array);
    let res2 = parser.validate('Product name,Price,Quantity \n Mollis consequat,Not,2');
    expect(res2.length).toBeGreaterThan(0);
    expect(res2).toBeInstanceOf(Array);
    let res3 = parser.validate('Product name,Price,Quantity \n Mollis consequat,3');
    expect(res3.length).toBeGreaterThan(0);
    expect(res3).toBeInstanceOf(Array);
    let res4 = parser.validate('Product name,Price,Quantity \n Mollis consequat, 3, String');
    expect(res4.length).toBeGreaterThan(0);
    expect(res4).toBeInstanceOf(Array);
    let res5 = parser.validate('Product name,Price,Quantity \n , 3, String');
    expect(res5.length).toBeGreaterThan(0);
    expect(res5).toBeInstanceOf(Array);
  });

  it('should return empty array when data is valid', () => {
    let res1 = parser.validate('Product name,Price,Quantity');
    expect(res1.length).toBe(0);
    expect(res1).toBeInstanceOf(Array);
    let res2 = parser.validate('Product name,Price,Quantity \n Mollis consequat,9.00,2');
    expect(res2.length).toBe(0);
    expect(res2).toBeInstanceOf(Array);
  });
});

describe('CartParser - integration test', () => {
  it('should return object with handle data', () => {
    expect(parser.parse('./samples/cart.csv')).toEqual(
      expect.objectContaining({
        items: expect.any(Array),
        total: expect.any(Number)
      })
    );
  });
});
